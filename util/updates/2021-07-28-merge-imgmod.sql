-- imgmod permissions merged into dbmod, no need to separate these.
ALTER TABLE users DROP COLUMN perm_imgmod;
